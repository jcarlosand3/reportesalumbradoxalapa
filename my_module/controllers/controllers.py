# -*- coding: utf-8 -*-
from odoo import http

# class /opt/odoo/odoo11/odoo11-custom-addons/myModule(http.Controller):
#     @http.route('//opt/odoo/odoo11/odoo11-custom-addons/my_module//opt/odoo/odoo11/odoo11-custom-addons/my_module/', auth='public')
#     def index(self, **kw):
#         return "Hello, world"

#     @http.route('//opt/odoo/odoo11/odoo11-custom-addons/my_module//opt/odoo/odoo11/odoo11-custom-addons/my_module/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('/opt/odoo/odoo11/odoo11-custom-addons/my_module.listing', {
#             'root': '//opt/odoo/odoo11/odoo11-custom-addons/my_module//opt/odoo/odoo11/odoo11-custom-addons/my_module',
#             'objects': http.request.env['/opt/odoo/odoo11/odoo11-custom-addons/my_module./opt/odoo/odoo11/odoo11-custom-addons/my_module'].search([]),
#         })

#     @http.route('//opt/odoo/odoo11/odoo11-custom-addons/my_module//opt/odoo/odoo11/odoo11-custom-addons/my_module/objects/<model("/opt/odoo/odoo11/odoo11-custom-addons/my_module./opt/odoo/odoo11/odoo11-custom-addons/my_module"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('/opt/odoo/odoo11/odoo11-custom-addons/my_module.object', {
#             'object': obj
#         })