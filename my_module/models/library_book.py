# -*- coding: utf-8 -*-

from odoo import models, fields, api
class LibraryBook(models.Model):
    _name = 'library.book'#identificador interno único para usar en toda la instancia de Odoo
    name = fields.Char('Title', required=True)
    date_release = fields.Date('Release Date')
    author_ids = fields.Many2many(
        'res.partner',
        string='Authors'
    )