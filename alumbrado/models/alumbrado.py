# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools

from odoo.addons.base_geoengine import geo_model
from odoo.addons.base_geoengine import fields as geo_fields
from datetime import datetime, timedelta

#----------------------LUMINARIAS----------------------#
class luminarias(geo_model.GeoModel):
    _name = 'alumbrado.luminarias'
    _rec_name = 'xap_comp'
    
    _sql_constraints = [
        ('xap_comp_uniq',
         'unique(xap_comp)',
         'Invoice Number must be unique per Company!'),
        ]

    #colonia = fields.Char(string="Colonia")   
    xap_comp = fields.Char(string="Código compuesto", compute="genera_cod_comp", store=True)
    cvecol = fields.Many2one('alumbrado.colonias', string="Clave de la colonia", required=True)
    xap = fields.Integer(string="Código de la luminaría", required=True)

    calle = fields.Char(string="Calle")
    referencia = fields.Char(string="Entre las calles")

    coordx = fields.Float(string="Coordenada X")
    coordy = fields.Float(string="Coordenada Y")
    ##
    geom = geo_fields.GeoPoint(string='Punto', srid=4326) 
    name = fields.Char('Serial number', size=64, required=True)#analizar
    ##
    tipocalle = fields.Char(string="Tipo de calle")
    tiposuelo = fields.Char(string="Tipo de suelo")
    ancalle = fields.Char(string="Ancho de calle")
    modelo = fields.Char(string="Modelo de lampara")
    tecnologia = fields.Char(string="Tipo de iluminación")
    potencias = fields.Char(string="Potencia consumida")
    marcas = fields.Char(string="Marca de foco")
    perdidas_a = fields.Integer(string="Perdidas")
    tension = fields.Char(string="tension")
    tipoposte = fields.Char(string="Tipo de poste")
    altoposte = fields.Float(string="Altura de poste")#numeric
    altolum = fields.Float(string="Altura de luminaria")#numeric
    brazo = fields.Float(string="Largo del brazo de la luminaria")#numeric
    
    fechaalta = fields.Date(string="Fecha del censo_1")###cambiar a fecha datos con /
    fcenso = fields.Date(string="Fecha censo")##cambiar a fecha
    
    tiposerv = fields.Char(string="Tipo de servicio cfe")
    zonascfe = fields.Char(string="Zonas de cfe")
    rpu = fields.Char(string="Rpu recibo cfe")
    medidor = fields.Char(string="Medidor cfe")

    estados = fields.Char(string="estado")
    
    def genera_cod_comp(self, values, cvecol):
        values["xap_comp"] = str(cvecol) +'-' +str(values.get('xap',self.xap))

    #def genera_reporte_no_repe(self, values, )

    def ConvertGeoJson(self, gjson, from_srid=4326, to_srid=3857):
        """
        H.Stivalet 2019-09-25
        Convierte el tipo de proyección en formato GeoJson usando funciones de PostGis. Requiere que la Base de datos de Odoo tenga instalada la estención de PosyGis
        params:
            gjson(str): GeoJson de un Point, Line o Polygon
            from_srid(int): SRID de entrada
            to_srid(int): SRID de salida
        returns: (str) GeoJson con la nueva proyección del Point, Line o Polygon
        """
        if not isinstance(gjson, str) or gjson == '':
            return ''
        query = "select ST_AsGeoJSON(st_transform(st_setsrid(st_geomfromgeojson('" + gjson + "')," + str(from_srid) + ")," + str(to_srid) + "))"
        self._cr.execute(query)
        data = self._cr.fetchone()
        if data:
            gjson = data[0]
        return gjson
    #------------------------------
    @api.model#necesita hacer algo en el nivel del modelo en sí, no solo algunos registros. #api.model para estructuras de modelos, métodos auxiliares, etc.
    def create(self, values):
        #------------------------------
        if 'geom' in values and values['geom'] != '':
            values['geom'] = self.env['alumbrado.luminarias'].ConvertGeoJson(values['geom'], 3857, 4326)
        #------------------------------

        self.genera_cod_comp(values, values['cvecol'])
        return super(luminarias, self).create(values)


    @api.multi#api.multi se usa para funciones en las que desea realizar alguna operación en uno o varios registros. Un ejemplo aquí puede ser establecer un campo en múltiples registros:
    def write(self, values):
        #------------------------------
        if 'geom' in values and values['geom'] != '':
            values['geom'] = self.ConvertGeoJson(values['geom'], 3857, 4326)
        #------------------------------
        if 'cvecol' in values or 'xap' in values:
            if values["cvecol"] != self.cvecol or values["xap"] != self.xap:
                self.genera_cod_comp(values,values["cvecol"] )
        return super(luminarias, self).write(values)

    #------------------------------
    @api.multi
    def read(self, fields=None, load='_classic_read'):
        record = super(luminarias, self).read(fields, load=load)
        if len(record) == 1 and 'geom' in record[0] and record[0]['geom'] != '':###record[0]['ageb_polygon'] != '':
            record[0]['geom'] = self.ConvertGeoJson(record[0]['geom'])
        return record
    #------------------------------
#----------------------COLONIAS----------------------#
class colonias(models.Model):
    _name = 'alumbrado.colonias'
    _rec_name = 'colonia'

    colonia = fields.Char(string="Colonia")
    cvecol = fields.Integer(string="Clave de Colonia")

#----------------------REPORTES----------------------#
class reportes(models.Model):
    _name = 'alumbrado.reportes'
    _rec_name = 'estatus'

    reporte_id = fields.Integer(string="id reporte")
    
    solicitante = fields.Many2one(comodel_name='res.partner', string="Solicitante") 
    estatus = fields.Selection([('nuevo','Nuevo'),('proceso','En proceso'),('atendida','Atendido')], string="Estatus del Reporte", required=True, default="nuevo", track_visibility=True) 
    medio_repor = fields.Selection([('T','twitter'),('D','Documento'),('L','Llamada Telefonica'),('P','En persona')], string="Medio de Solicitud")
    create_date = fields.Datetime(comodel_name='res.partner', string="Fecha")
    
    luminaria = fields.One2many('alumbrado.rel_reportes', 'reporte_id' )

#----------------------RELACION DE REPORTES----------------------#
class rel_reportes_alumbrado(models.Model):
    _name = 'alumbrado.rel_reportes'
    _rec_name = 'estatus'  
     
    #name = fields.Many2one(comodel_name='alumbrado.reportes', string="Folio Reporte")
    reporte_id = fields.Many2one(comodel_name='alumbrado.reportes', string="Folio Reporte")
    motivo = fields.Selection([('Apagado','Apagado'),('Encendido','Encendido'),('Intermitente','Intermitente'),('Luz Baja','Luz Baja')], string="Motivo de Peticion", required=True) 
    description = fields.Text(string='Descripcion detallada')
    luminaria = fields.Many2one(comodel_name='alumbrado.luminarias', string="Luminaria", required=True) 
    estatus = fields.Many2one(comodel_name='alumbrado.reportes', string="Estatus del Reporte")


#----------------------ATENCION DE REPORTES----------------------#
class atencion_reportes(models.Model):
    _name = 'alumbrado.atencion_reportes'
    #_rec_name = 'numero'

    id = fields.Many2one(comodel_name='alumbrado.reportes', string="numero de reporte")#como poner el id
    no_reporte = fields.Many2one(comodel_name='alumbrado.reportes', string="numero de reporte")
    numero = fields.Many2one(comodel_name='alumbrado.vehiculo', string="numero de vehiculo")
    turno = fields.Many2one(comodel_name='alumbrado.jefe_cuadrilla', string="Turno de jefe de Cuadrilla")
    cuadrilla = fields.Many2one(comodel_name='res.partner', string="Jefe de Cuadrilla")

#----------------------REPUESTAS DE ATENCION DE REPORTES----------------------#
class respuestas_atencion(models.Model):
    _name = 'alumbrado.respuestas_atencion'

    estatus = fields.Many2one(comodel_name='alumbrado.reportes', string="Estatus")
    material = fields.Many2one(comodel_name='alumbrado.almacen', string="material del almacen") 
    motivo = fields.Selection([('B','Falta Balastra'),('C','Falta Cable'),('F','Falta Grua'),('G','Falta Escalera Grande'),('F','Falta Foco'),('M','Falta Material'),('P','Falta Poste')], string="Motivo No se atendio")

########################################################################################
class almacen(models.Model):
    _name = 'alumbrado.almacen'

    material = fields.Char(string="material del almacen")
    vale = fields.Char(string="numero de vale")
    factura = fields.Char(string="numero de factura")
    proveedor = fields.Char(string="Preveedor")#¿provedor se pone en res.partner?
    recibe = fields.Char(string="nombre de quien recibe material")#¿recibe se pone en res.partner?


class vehiculo(models.Model):
	_name = 'alumbrado.vehiculo'
    #_rec_name = 'numero'

	numero = fields.Char(string="numero del vehiculo")
	tipoVehiculo = fields.Selection([('C','Camioneta'),('G','Grua Normal'),('A','Corto Alcance')], string="Tipo de vehiculo")
	marca = fields.Char(string="Marca del Vehiculo")
	modelo = fields.Date(string="Modelo del vehiculo")
	placas = fields.Char(string="numero de placas")

class jefe_cuadrilla(models.Model):
    _name = 'alumbrado.jefe_cuadrilla'
    _rec_name = 'turno'

    cuadrilla = fields.Many2one(comodel_name='res.partner', string="Jefe de Cuadrilla")
    numero = fields.Many2one(comodel_name='alumbrado.vehiculo', string="numero de vehiculo")
    telefono = fields.Char(string="telefono del Jefe de cuadrilla", related="cuadrilla.phone")
    turno = fields.Selection([('M','Matutino'),('V','Vespertino'),('F','Fin de Semana'),('S','Sabado')], string="Turno de jefe de Cuadrilla")

'''
 ###autocompletado Reportes
    #luminaria = fields.Many2one(comodel_name='alumbrado.luminarias', string="Luminaria")#Objeto
    # xap_comp = fields.Char(string="xap compuesto", related="luminaria.xap_comp", store = True)
    # cvecol = fields.Integer(string="clave colonia", related="luminaria.cvecol")
    # xap = fields.Integer(string="xap", related="luminaria.xap")
    # calle = fields.Char(string="calle", related="luminaria.calle")
    # cvecalle = fields.Integer(string="clave de calle", related="luminaria.cvecalle")
    # entre = fields.Char(string="entre", related="luminaria.entre")
 '''
